"""l_log URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# user requests ## p413
from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path(
        'admin/'
        , admin.site.urls
    ),
    
    # include users ## p439
    path(
        'users/'
        , include('users.urls')
    ),

    # include URLs from l_logs ## p413
    path(
        ''
        , include('l_logs.urls')
    ),
]
