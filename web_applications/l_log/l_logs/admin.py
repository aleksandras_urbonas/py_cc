from django.contrib import admin

# Register your models here.

# import model Topic ## p407 p409
from l_logs.models import Topic, Entry

# register our model via admin site ## p407
admin.site.register(Topic)
# register our model via admin site ## p409
admin.site.register(Entry)
