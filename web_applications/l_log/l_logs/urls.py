"""Define URL patterns for l_logs."""

# user requests ## p413
from django.urls import path

# map URL to views ## p413
from . import views

# identify app ## p413
app_name = 'l_logs'

# list of pages accesible for this app ## p413
urlpatterns = [
    # home page ## p413
    path(
        ''
        , views.index
        , name='index'
    ),

    # show all topics. # p419
    path(
        'topics/'
        , views.topics
        , name='topics'
    ),

    # detail page for a single topic. ## p422
    path(
        'topics/<int:topic_id>/'
        , views.topic
        , name='topic'
    ),

    # page for adding a new topic. ## p429
    path(
        'new_topic/'
        , views.new_topic
        , name='new_topic'
    ),

    # page for adding a new entry. ## 433
    path(
        'new_entry/<int:topic_id>/'
        , views.new_entry
        , name='new_entry'
    ),

    # page for editing an entry. ## 436
    path(
        'edit_entry/<int:entry_id>/'
        , views.edit_entry
        , name='edit_entry'
    ),
]
