### Allow users to enter data

## p428
from django import forms


## p428 p432
from .models import Topic, Entry


# ModelForm for Topic ## p428
class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = ['text']
        labels = {'text': ''}


# ModelForm for Entry ## p432
class EntryForm(forms.ModelForm):
    class Meta:
        model = Entry
        fields = ['text']
        labels = {'text': ''}
        widgets = {'text': forms.Textarea(attrs={'cols':80})}