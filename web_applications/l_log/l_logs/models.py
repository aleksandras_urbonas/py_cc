# Create your models here.
# modify models, then migrate the db ## p409

from django.db import models


## p403
class Topic(models.Model):
	"""A topic the user is learning about."""
	text = models.CharField(
        max_length=200
        )
	date_added = models.DateTimeField(
        auto_now_add=True
        )
	
	def __str__(self):
		"""Return a string representation of the model."""
		return self.text


## p408
class Entry(models.Model):
	"""Something specific learned about topic."""
	topic = models.ForeignKey(
        Topic
        , on_delete=models.CASCADE
        )
	text = models.TextField()
	date_added = models.DateTimeField(
        auto_now_add=True
        )
	
	class Meta:
		verbose_name_plural = 'entries'
	
	def __str__(self):
		"""Return a string representation of the model."""
		if len(self.text) > 50:
			text = self.text[0:50]
		else:
			text = self.text
			
		return text