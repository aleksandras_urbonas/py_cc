# Create your views here.

from django.shortcuts import render
# new topic ## p429
from django.http import HttpResponseRedirect
# new topic ## p429
from django.urls import reverse


# topic ## p419 p436
from .models import Topic, Entry
# new topic ## p429 p433
from .forms import TopicForm, EntryForm


# render home page ## p415
def index(request):
    """Home page for l_log."""
    return render(
        request
        , 'l_logs/index.html'
        )


# render topics page. ## p419
def topics(request):
    """Show all topics."""
    topics = Topic.objects.order_by('date_added')
    context = {'topics': topics}
    return render(
        request
        , 'l_logs/topics.html'
        , context
        )


# render single topic page. ## p422
def topic(
    request
    , topic_id
    ):
    """Show a single topic with all entries."""
    topic = Topic.objects.get(id=topic_id)
    entries = topic.entry_set.order_by('-date_added')
    context = {
        'topic': topic
        , 'entries': entries
        }
    return render(
        request
        , 'l_logs/topic.html'
        , context
        )


# new topic ## p429
def new_topic(request):
    """Add a new topic."""
    if request.method != 'POST':
        # No data submitted, create a blank form.
        form = TopicForm()
    else:
        # POST data submitted; process data.
        form = TopicForm(
            data=request.POST
            )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                reverse('l_logs:topics')
                )

    context = {'form': form}
    return render(
        request
        , 'l_logs/new_topic.html'
        , context
        )


# new topic ## p429
def new_entry(
    request
    , topic_id
    ):
    """Add a new entry for specific topic."""
    topic = Topic.objects.get(id=topic_id)
    
    if request.method != 'POST':
        # No data submitted, create a blank form.
        form = EntryForm()
    else:
        # POST data submitted; process data.
        form = EntryForm(
            data=request.POST
            )
        if form.is_valid():
            new_entry = form.save(commit=False)
            new_entry.topic = topic
            new_entry.save()
            return HttpResponseRedirect(
                reverse(
                    'l_logs:topic'
                    , args=[topic_id]
                    )
                )

    context = {'topic': topic, 'form': form}
    return render(
        request
        , 'l_logs/new_entry.html'
        , context
        )


# edit entry ## p436
def edit_entry(request, entry_id):
    """Edit an existing entry."""
    entry = Entry.objects.get(id=entry_id)
    topic = entry.topic
    
    if request.method != 'POST':
        # Initial request; pre-fill form with current entry.
        form = EntryForm(instance=entry)
    else:
        # POST data submitted; process data.
        form = EntryForm(
            instance=entry
            , data=request.POST
            )
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(
                reverse(
                    'l_logs:topic'
                    , args=[topic.id]
                    )
                )

    context = {
        'entry': entry
        , 'topic': topic
        , 'form': form
        }
    return render(
        request
        , 'l_logs/edit_entry.html'
        , context
        )