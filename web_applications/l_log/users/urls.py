"""Defines URL patterns for users."""

# user requests ## p440
from django.urls import path
from django.contrib.auth import login

from . import views


app_name = 'users'

urlpatterns = [
    # login page ## p440
    path(
        'login/'
        , login
        #, {'template_name': 'users/login.html'}
        , name='login'
    ),
    path(
        'logout/'
        , views.logout_view
        , name='logout'
    ),
]
