# logout ## p443
from django.contrib.auth import authenticate, login, logout

from django.http import HttpResponseRedirect
from django.urls import reverse


def my_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.

        if request.user.is_authenticated:
            # Do something for authenticated users.
            #...
        else:
            # Do something for anonymous users.
            #...
    else:
        # Return an 'invalid login' error message.
        #...

# logout view ## p443
def logout_view(request):
    """Log the user out."""

    # call the logout ## p443
    logout(request)
    # redirect to success page ## p443
    return HttpResponseRedirect(
        reverse('l_logs:index')
    )
