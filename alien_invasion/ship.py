### Alien Invasion: AU edition
# ship.py


import pygame
from pygame.sprite import Sprite ## p313


## p313
class Ship(Sprite):
	"""Initialize ship and set the starting position."""

	def __init__(self, ai_settings, screen):
		# inherit ship from sprite ## p313
		super(Ship, self).__init__()
		self.screen = screen
		self.ai_settings = ai_settings
	
		# load the ship ## p245
		self.image = pygame.image.load('images/ship.png').convert_alpha()
		self.rect = self.image.get_rect()
		self.screen_rect = screen.get_rect()
	
		# start each new ship at the bottom center
		self.rect.centerx = self.screen_rect.centerx
		self.rect.bottom = self.screen_rect.bottom
		
		# store a decimal value for the ship's center ## p253
		self.center = float(self.rect.centerx)
		# store a decimal value for the ship's vertical position ## p257
		#self.centery = float(self.rect.centery)
		
		# continous movement flag ## p. 250
		self.moving_left = False
		self.moving_right = False
		#self.moving_up = False
		#self.moving_down = False

	
	## p. 250 allow continuous movement
	def update(self):
		"""Update the ship's position based on the movement flag."""
		# update the ship's center, not the rect. ## p254
		# and check screen limits ## p255
		if self.moving_right and self.rect.right < self.screen_rect.right:
			self.center += self.ai_settings.ship_speed_factor
		if self.moving_left and self.rect.left > 0:
			self.center -= self.ai_settings.ship_speed_factor
		# add moving up ## p257
		#if self.moving_up and self.rect.top < self.screen_rect.top:
		#	self.centery += self.ai_settings.ship_speed_factor
		#if self.moving_down and self.rect.bottom > 0:
		#	self.centery -= self.ai_settings.ship_speed_factor
		
		# update object `rect` from `self.center`
		self.rect.centerx = self.center
		# self.rect.centery = self.centery


	def blitme(self):
		"""Draw the ship at its current location"""
		self.screen.blit(self.image, self.rect)


	def center_ship(self):
		"""Center the ship on the screen."""
		self.center = self.screen_rect.centerx
