### Alien Invasion: AU edition
# alien.py ## p267


import pygame
from pygame.sprite import Sprite


class Alien(Sprite):
	"""Class manages a single alien."""

	def __init__(self, ai_settings, screen):
		"""Create an alien object and set start position."""
		super(Alien, self).__init__()
		self.screen = screen
		self.ai_settings = ai_settings
		
		# Loop the alien image and set its rect attribute. ## p267
		self.image = pygame.image.load('images/alien_2.png').convert_alpha()
		self.rect = self.image.get_rect()
		
		# start each new alien at the top left of the screen
		self.rect.x = self.rect.width
		self.rect.y = self.rect.height
		
		# store the alien's exact position
		self.x = float(self.rect.x)


	# draw alien ## p259
	def blitme(self):
		"""Draw the alien at its current position."""
		self.screen.blit(self.image, self.rect)


	# check if alien on the edge ## p278
	def check_edges(self):
		"""Return `True` if alien is at edge."""
		screen_rect = self.screen.get_rect()
		if self.rect.right >= screen_rect.right:
			return True
		elif self.rect.left <= 0:
			return True


	# move alien ## p276
	def update(self):
		"""Update the alien's position based on the movement flag."""
		# move the alien to the right
		self.x += (self.ai_settings.alien_speed_factor * self.ai_settings.fleet_direction)
		# update object `rect` from `self.center`
		self.rect.x = self.x


