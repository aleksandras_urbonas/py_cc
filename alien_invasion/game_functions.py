#### Invasion: AU edition
# game_functions.py


import sys
import pygame
from bullet import Bullet ## p260
from alien import Alien ## p271
from time import sleep ## p286


## p274
def get_number_rows(ai_settings, ship_height, alien_height):
	"""Determine number of rows of aliens to fit on screen."""
	available_space_y = (ai_settings.screen_height - (3 * alien_height) - ship_height)
	number_rows = int(available_space_y / (2 * alien_height))
	return number_rows


## p273
def get_number_aliens(ai_settings, alien_width):
	"""Determine number of aliens to fit in row."""
	available_space_x = ai_settings.screen_width - 2 * alien_width
	number_aliens_x = int(available_space_x / (2 * alien_width))
	return number_aliens_x


## p273
def create_alien(ai_settings, screen, aliens, alien_number, row_number):
	"""Create alien and place it in a row."""
	alien = Alien(ai_settings, screen)
	# spacing between aliens is equal to one alien width
	alien_width = alien.rect.width
	alien.x = alien_width + 2 * alien_width * alien_number
	alien.rect.x = alien.x
	## p274
	alien.rect.y = alien.rect.height + 2 * alien.rect.height * row_number
	aliens.add(alien)


## p271
def create_fleet(ai_settings, screen, ship, aliens):
	"""Create full alien fleet."""

	# create alien and find number of aliens in row.
	alien = Alien(ai_settings, screen)
	number_aliens_x = get_number_aliens(ai_settings, alien.rect.width)
	number_rows = get_number_rows(ai_settings, ship.rect.height, alien.rect.height)

	# Create the first row of aliens. ## p273 p274
	for row_number in range(number_rows):
		for alien_number in range(number_aliens_x):
			# create alien and place alien in row.
			create_alien(ai_settings, screen, aliens, alien_number, row_number)


## p260 p294
def update_screen(
	ai_settings
	, screen
	, stats
	, sb ## p303
	, ship
	, aliens
	, bullets
	, play_button
):
	"""Update images on screen and flip to the new screen."""

	# Redraw the screen during each pass
	screen.fill(ai_settings.bg_color)

	# draw the score information ## p303
	sb.show_score()

	# Redraw all bullets behind the ship and aliens ## p260
	for bullet in bullets.sprites():
		bullet.draw_bullet()

	# Draw ship
	ship.blitme()

	# Draw alien ## p268 p271
	aliens.draw(screen)

	# show play button ## p294
	if not stats.game_active:
		play_button.draw_button()

	# make the most recently drawn screen visible
	pygame.display.flip()


## p260 p295
def check_events(
	ai_settings
	, screen
	, stats
	, sb ## p311
	, play_button
	, ship
	, aliens
	, bullets
	):
	"""Respond to keypresses and mouse events."""
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit()
		# key references: https://www.pygame.org/docs/ref/key.html
		elif event.type == pygame.KEYDOWN:
			check_keydown_events(event, ai_settings, screen, ship, bullets)
		elif event.type == pygame.KEYUP:
			check_keyup_events(event, ai_settings, screen, ship, bullets)
		# play button click with mouse ## p295
		elif event.type == pygame.MOUSEBUTTONDOWN:
			mouse_x, mouse_y = pygame.mouse.get_pos()
			check_play_button(
				ai_settings
				, screen
				, stats
				, sb ## p311
				, play_button
				, ship
				, aliens
				, bullets
				, mouse_x
				, mouse_y
				)


## p295 p296
def check_play_button(
	ai_settings
	, screen
	, stats
	, sb ## p311
	, play_button
	, ship
	, aliens
	, bullets
	, mouse_x
	, mouse_y
	):
	"""Start a new game when the player clicks Play."""

	## p297
	button_clicked = play_button.rect.collidepoint(mouse_x, mouse_y)
	if button_clicked and not stats.game_active:
		# Reset the game settings():
		ai_settings.initialize_dynamic_settings()
		
		# hide the mouse cursor:
		pygame.mouse.set_visible(False)

		# Reset game stats.
		stats.reset_stats()
		stats.game_active = True
		
		# Reset the scoreboard images. ## p312
		sb.prep_score()
		sb.prep_high_score()
		sb.prep_level()
		sb.prep_ships() ## p315

		# empty alien and bullets list
		aliens.empty() 
		bullets.empty() 

		# create a new fleet and center the ship.
		create_fleet(ai_settings, screen, ship, aliens)
		ship.center_ship()


## p260
def check_keydown_events(event, ai_settings, screen, ship, bullets):
	"""Responds to a pressed key."""
	#print(event.key) # key code (308=ALT,276=LEFT)

	# right = move right
	if event.key == pygame.K_RIGHT:
		ship.moving_right = True
	# left = move left
	elif event.key == pygame.K_LEFT:
		ship.moving_left = True
	# space = fire bullet
	elif event.key == pygame.K_SPACE:
		# fire bullet ## p264
		fire_bullet(ai_settings, screen, ship, bullets)
	# q = exit ## p266
	elif event.key == pygame.K_q:
		sys.exit()


def check_keyup_events(event, ai_settings, screen, ship, bullets):
	"""Responds to a released key."""
	# right
	if event.key == pygame.K_RIGHT:
		ship.moving_right = False
	# left
	elif event.key == pygame.K_LEFT:
		ship.moving_left = False


# update bullets, including collisions ## p280 p282
def update_bullets(
	ai_settings
	, screen
	, stats ## p305
	, sb ## p305
	, ship
	, aliens
	, bullets
	):
	"""Update position of bullets and get rid of old bullets."""
	# update bullets ## p260
	bullets.update()

	# get rid of bullets that have dissapered ## p262
	for bullet in bullets.copy():
		if bullet.rect.bottom <= 0:
			bullets.remove(bullet)

	# check bullet and alien collisions ## p283
	check_bullet_alien_collisions(
		ai_settings
		, screen
		, stats ## p305
		, sb ## p305
		, ship
		, aliens
		, bullets
		)
	#print(len(bullets)) # qc ## p256


def check_bullet_alien_collisions(
	ai_settings
	, screen
	, stats ## p305
	, sb ## p305
	, ship
	, aliens
	, bullets
	):
	"""Respond to bullet-alien collisions ## p283"""
	# remove any bullet-alien collision ## p283
	collisions = pygame.sprite.groupcollide(bullets, aliens, True, True)
	
	# if alien hit with bullet, update the score ## p305
	if collisions:
		# loop all collisions ## p306
		for aliens in collisions.values():
			# score for each alien hit
			stats.score += ai_settings.alien_points * len(aliens)
			sb.prep_score()
		# check high score ## p309
		check_high_score(stats, sb)

	# if all aliens are destroyed, start a new level.
	if len(aliens) == 0:
		# destroy existing bullets and create new fleet
		bullets.empty()
		ai_settings.increase_speed()

		# increase level ## p311
		stats.level += 1
		sb.prep_level()

		create_fleet(ai_settings, screen, ship, aliens)


def fire_bullet(ai_settings, screen, ship, bullets):
	# check if allowed number of bullets
	if len(bullets) < ai_settings.bullets_allowed:
		# create a new bullet
		new_bullet = Bullet(ai_settings, screen, ship)
		# add bullet to the bullets group
		bullets.add(new_bullet)


# update alien direction ## p278
def check_fleet_edges(ai_settings, aliens):
	"""Respond if any aliens on the edge."""
	for alien in aliens.sprites():
		if alien.check_edges():
			change_fleet_direction(ai_settings, aliens)
			break


# change direction of alien fleet ## p279
def change_fleet_direction(ai_settings, aliens):
	"""Drop the entire fleet and change direction."""
	for alien in aliens.sprites():
		alien.rect.y += ai_settings.fleet_drop_speed
	# reverse ## p279
	ai_settings.fleet_direction *= -1


# ship hit ## p287
def ship_hit(
	ai_settings
	, screen
	, stats
	, sb ## p315
	, ship
	, aliens
	, bullets
	):
	"""Respond to ship being hit by alien."""

	if stats.ships_left > 0:

		# decrease ships_left:
		stats.ships_left -= 1
		
		# Update scoreboard. ## p315
		sb.prep_ships()

		# empty alien and bullets list
		aliens.empty() 
		bullets.empty() 

		# create a new fleet and center the ship.
		create_fleet(ai_settings, screen, ship, aliens)
		ship.center_ship()

		# Pause.
		sleep(0.5)
	else:
		stats.game_active = False
		# show mouse ## p298
		pygame.mouse.set_visible(True)


# update aliens ## p277
def update_aliens(
	ai_settings
	, screen
	, stats
	, sb ## p315
	, ship
	, aliens
	, bullets
	):
	"""
	Check if the fleet is at the edge,
	then update positions of all fleet aliens.
	"""
	check_fleet_edges(ai_settings, aliens)
	# update ## ..
	aliens.update()

	# look for alien-ship collisions ## p284 p287
	if pygame.sprite.spritecollideany(ship, aliens):
		ship_hit(
			ai_settings
			, screen
			, stats
			, sb ## p315
			, ship
			, aliens
			, bullets
			)

	# look for aliens hitting the bottom of the screen
	check_aliens_bottom(
		ai_settings
		, screen
		, stats
		, sb ## p315
		, ship
		, aliens
		, bullets
		)


# check if aliens reached screen bottom ## p288
def check_aliens_bottom(
	ai_settings
	, screen
	, stats
	, sb ## p316
	, ship
	, aliens
	, bullets
	):
	"""Check if any aliens have reached the bottom of the screen."""
	screen_rect = screen.get_rect()
	for alien in aliens.sprites():
		if alien.rect.bottom >= screen_rect.bottom:
			# treat this the same as if the ship got hit
			ship_hit(
				ai_settings
				, screen
				, stats
				, sb ## p316
				, ship
				, aliens
				, bullets
				)
			break


# check high score ## p309
def check_high_score(stats, sb):
	"""Check for new high score."""
	if stats.score > stats.high_score:
		stats.high_score = stats.score
		sb.prep_high_score()
