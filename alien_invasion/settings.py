### Alien Invasion: AU edition
# settings.py


from pygame.locals import Color


class Settings():
	"""Class stores all game settings."""

	def __init__(self):
		"""Initialize game settings."""

		## Screen settings
		self.screen_width = 800
		self.screen_height = 600
		self.screen_size = (self.screen_width, self.screen_height)
		#self.bg_color = (230, 230, 230) # original, grey
		#self.bg_color = (0, 0, 200) # test, blue: ship not transparent
		self.bg_color = Color('white') # test module `Color`

		## Ship settings
		# ship limit ## p286
		self.ship_limit = 3

		## Bullet settings
		self.bullet_width = 3
		self.bullet_height = 15
		self.bullet_color = 60, 60, 60
		self.bullets_allowed = 3

		## Alien settings
		# fleet drop speed ## p277
		self.fleet_drop_speed = 10

		## Game settings ## p299
		# game speedup
		self.speedup_scale = 1.1
		# alien points speedup ## p306
		self.score_scale = 1.5
		
		self.initialize_dynamic_settings()


	def initialize_dynamic_settings(self):
		"""Initialize settings that change during game."""
		# speed ## p253 p299
		self.ship_speed_factor = 1.5
		# speed ## p283 p299
		self.bullet_speed_factor = 3
		# speed ## p276 p300
		self.alien_speed_factor = 0.8
		# fleet direction `1` = right, and `-1` = left # .. p299
		self.fleet_direction = 1
		
		## Scoring
		# points for shot alien ## p304
		self.alien_points = 50


	# speed settings ## p300
	def increase_speed(self):
		"""Increase speed."""
		self.ship_speed_factor *= self.speedup_scale
		self.bullet_speed_factor *= self.speedup_scale
		self.alien_speed_factor *= self.speedup_scale

		self.alien_points = int(self.alien_points * self.score_scale)
