### Alien Invasion: AU edition
# bullet.py ## p258


import pygame
from pygame.sprite import Sprite


class Bullet(Sprite):
	"""Class manages fired ship bullets."""

	
	def __init__(self, ai_settings, screen, ship):
		"""Create a bullet object at ship's current position"""
		super(Bullet, self).__init__()
		self.screen = screen
		
		# create a bullet rect at (0,0) and then set the correct place
		self.rect = pygame.Rect(0, 0, ai_settings.bullet_width, ai_settings.bullet_height)
		self.rect.centerx = ship.rect.centerx
		self.rect.top = ship.rect.top
		
		# store bullet position as decimal
		self.y = float(self.rect.y)
		
		self.color = ai_settings.bullet_color
		self.speed_factor = ai_settings.bullet_speed_factor


	# update position ## p259
	def update(self):
		"""Move the bullet up the screen."""
		# update the decimal position of the bullet.
		self.y -= self.speed_factor
		# update the rect position.
		self.rect.y = self.y


	# draw ## p259
	def draw_bullet(self):
		"""Draw the bulet to the screen."""
		pygame.draw.rect(self.screen, self.color, self.rect)
