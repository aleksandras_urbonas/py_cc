### Alien Invasion: AU edition
# game_stats.py ## p285


class GameStats():
	"""Track statistics for Alien Invasion."""
	
	def __init__(self, ai_settings):
		"""Initialize statistics."""
		self.ai_settings = ai_settings
		self.reset_stats()

		# start the game in inactive status ## p288 p292
		self.game_active = False
		
		# high score should never be reset ## p308
		self.high_score = 0


	def reset_stats(self):
		"""Initialize statistics that can change during game."""
		self.ships_left = self.ai_settings.ship_limit
		# score ## p301
		self.score = 0
		# level ## p310
		self.level = 1
