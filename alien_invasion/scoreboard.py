### Alien Invasion: AU edition
# scoreboard.py


import pygame.font
from pygame.sprite import Group ## p314

from ship import Ship ## p314


class Scoreboard():
	"""Class reports all game scores."""

	def __init__(
		self
		, ai_settings
		, screen
		, stats
		):
		"""Initialize scorekeeping attributes."""

		## Screen settings
		self.screen = screen
		self.screen_rect = screen.get_rect()
		self.ai_settings = ai_settings
		self.stats = stats
		
		# font settings for scoring information
		self.text_color = (30, 30, 30)
		self.font = pygame.font.SysFont(None, 48)
		
		# Prepare the initial score image
		self.prep_score()
		# prepare high score ## p308
		self.prep_high_score()
		# prepare level ## p310
		self.prep_level()
		# prepare ships ## p314
		self.prep_ships()


	# prepare scores ## p302
	def prep_score(self):
		"""Render score as image."""
		
		# format points ## p307
		rounded_score = int(round(self.stats.score, -1))
		score_str = "{:,}".format(rounded_score)
		self.score_image = self.font.render(
			score_str
			, True
			, self.text_color
			, self.ai_settings.bg_color
			)
		
		# Display the score at screen top right
		self.score_rect = self.score_image.get_rect()
		self.score_rect.right = self.screen_rect.right - 20
		self.score_rect.top = 20


	def show_score(self):
		"""Draw scores and ships to the screen."""

		# score ## p30x
		self.screen.blit(
			self.score_image
			, self.score_rect
			)

		# high score ## p309
		self.screen.blit(
			self.high_score_image
			, self.high_score_rect
			)

		# level ## p311
		self.screen.blit(
			self.level_image
			, self.level_rect
			)
		# ships left ## p314
		self.ships.draw(self.screen)


	# prepare high score ## p308
	def prep_high_score(self):
		"""Render high score as image."""
		
		high_score = int(round(self.stats.high_score, -1))
		high_score_str = "{:,}".format(high_score)
		self.high_score_image = self.font.render(
			high_score_str
			, True
			, self.text_color
			, self.ai_settings.bg_color
			)
		
		# Display the score at screen top right
		self.high_score_rect = self.high_score_image.get_rect()
		self.high_score_rect.centerx = self.screen_rect.centerx
		self.high_score_rect.top = self.score_rect.top


	# prepare level ## p311
	def prep_level(self):
		"""Turn the level into a rendered image."""
		# render level image
		self.level_image = self.font.render(
			str(self.stats.level)
			, True
			, self.text_color
			, self.ai_settings.bg_color
			)
		
		# position the level below the score.
		self.level_rect = self.level_image.get_rect()
		self.level_rect.right = self.score_rect.right
		self.level_rect.top = self.score_rect.bottom + 10


	# prepare number of ships ## p314
	def prep_ships(self):
		"""Show how many ships are left."""
		
		self.ships = Group()
		
		for ship_number in range(self.stats.ships_left):
			ship = Ship(
				self.ai_settings
				, self.screen
				)
			ship.rect.x = 10 + ship_number * ship.rect.width
			ship.rect.y = 10
			self.ships.add(ship)
