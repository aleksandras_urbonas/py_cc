### Alien Invasion: AU edition
# alien_invasion.py


import sys
import pygame
import game_functions as gf
from pygame.sprite import Group ## p259
from settings import Settings
from ship import Ship
from bullet import Bullet ## p257
from alien import Alien ## p268
from game_stats import GameStats ## p286
from button import Button ## p294
from scoreboard import Scoreboard ## p303


def run_game():
	"""Main game logics."""

	# initialize game
	pygame.init()

	# add settings
	ai_settings = Settings()

	# set display mode
	screen = pygame.display.set_mode(ai_settings.screen_size)

	# set display caption
	pygame.display.set_caption('Alien Invasion: AU edition')
	
	# display `Play` button ## p294
	play_button = Button(
		ai_settings
		, screen
		, "Play"
	)

	# Create an instance to store game statistics ## p286
	stats = GameStats(ai_settings)

	# Create scoreboard ## p303
	sb = Scoreboard(
		ai_settings
		, screen
		, stats
	)

	# Make an alien. ## p268
	alien = Alien(ai_settings, screen)

	# Make a ship, bullets group, and aliens group ## p270
	# make a ship ## p246
	ship = Ship(ai_settings, screen)

	# make a group of bullets ## p259
	bullets = Group()
	aliens = Group() ## p270
	
	# create alien fleet ## p270 p274
	gf.create_fleet(
		ai_settings
		, screen
		, ship
		, aliens
	)
	
	# Start the main loop for the game
	while True:
		# monitor for mouse and keyboard events ## .. p259 p296
		gf.check_events(
			ai_settings
			, screen
			, stats
			, sb ## p312
			, play_button
			, ship
			, aliens
			, bullets
			)
		
		# check if game is active ## p289
		if stats.game_active:

			# update ship loop ## p251
			ship.update()
			
			# update bullets ## p263 p283
			gf.update_bullets(
				ai_settings
				, screen
				, stats ## p305
				, sb ## p305
				, ship
				, aliens
				, bullets
			)
			
			# update aliens ## p277
			gf.update_aliens(
				ai_settings
				, screen
				, stats
				, sb ## p316
				, ship
				, aliens
				, bullets
			)

		# display elements on screen ## .. p260 p294 p296 
		gf.update_screen(
			ai_settings
			, screen
			, stats
			, sb # scoreboard ## p303
			, ship
			, aliens
			, bullets
			, play_button
		)

# main
run_game()
