#### Aleksandras Urbonas, 2018-08-09
title = 'Python Crash Course'
chapter = '3'
chapter_name = 'Lists'
print('' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')

# create my list
my_list = [
'apples'
,'bananas'
,'water'
,'buckwheat'
]

'''
### completed
## create a list, empty
my_list = []

## p. 38: print a simple list
# print my list
print(
'my_list:\n'
, my_list
)

## p. 38: print a single list item
# last item: str(-1)
item = 1
list_item = item - 1
print(
'my_list, item #' + str(list_item) + ':\n'
, my_list[list_item].title()
)

## p. 39: print last item
print(
'I prefer ' +
my_list[-1].title() +
' for lunch!'
)

## p. 41 update list item
# replace water with eggs
my_list[2] = 'eggs'
print(
'my _updated_ list:\n'
, my_list
)

## p. 41 add list item to end
# add salt
my_list.append('salt')
print(
'my _longer_ list:\n'
, my_list
)

## p. 42 insert list element
# insert water to beginning
my_list.insert(0, 'water')
print(
'my _reordered_ list:\n'
, my_list
)

## p. 42 remove list element
# remove water to beginning
print('removing ' + my_list[1] + ':\n')
del my_list[1]
print(
'my _shorter_ list:\n'
, my_list
)


## p. 44: remove and save last element with `pop`
my_popped_list = my_list.pop()
print(
'my last added element was:'
, my_popped_list
)

## p. 44: remove using `remove`
my_list.remove('apples')
print('no apples:',my_list)
too_sweet = 'bananas'
my_list.remove(too_sweet)
print(
too_sweet
, 'are too sweet:'
, my_list
)

## p. 47 sort a list
print(
'original list:\n'
,my_list
)
# actual sort (final)
my_list.sort()
print(
'sorted:\n'
,my_list
)

## p. 48 sort a list locally
print(
'original list:\n'
,my_list
)
print(
'sorted locally:\n'
,sorted(my_list)
)
print(
'original list again:\n'
,my_list
)

print(
'original list:\n'
,my_list
)
# reversing
my_list.reverse()
print(
'my reversed list:\n'
, my_list
)

## p. 49 reverse a list
## length of list
print(
'length of my list is'
, len(my_list)
)


## p. 51 list index error
# print non-existing element
# print(my_list[5]) # error

# print last element
print(my_list[-1])

# but no last element in empty list!
my_list = []
print(my_list[-1]) # error

'''
