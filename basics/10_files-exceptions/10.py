#### Aleksandras Urbonas, 2018-08-10
title = 'Python Crash Course'
chapter = '10'
chapter_name = 'Files and Exceptions'
print('~' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')



'''
### completed


## p. 190 read entire file
# file contains 30 digits of number `pi`
# path includes sub-folder
file_path = 'src/pi_digits.txt'

with open(file_path) as file_object:
	contents = file_object.read()
    # strip spaces and print
	print(contents.rstrip())


## p. 193 read entire file by line
file_path = 'src/pi_digits.txt'

with open(file_path) as file_object:
	for line in file_object:
		# print line including linefeeds and carriage returns
		#print('A: ', line)
		# strip spaces and print - single line
		print('B: ', line.rstrip())



## p. 194 make a list of lines from a file
file_path = 'src/pi_digits.txt'

with open(file_path) as file_object:
	lines = file_object.readlines()
	
for line in lines:
	print(line.rstrip())



## p. 194 working with file contents
file_path = 'src/pi_digits.txt'

with open(file_path) as file_object:
	lines = file_object.readlines()

# create empty string
pi_string = ''

# create empty string
for line in lines:
	pi_string += line.strip() # instead of rstrip

print(pi_string)
print(len(pi_string))


## p. 195 large files: one million digits
filename = 'src/pi_million_digits.txt'

with open(filename) as file_object:
	lines = file_object.readlines()

# create empty string
pi_string = ''

# create empty string
for line in lines:
	pi_string += line.strip() # instead of rstrip

print(pi_string[:52] + '...')
print(len(pi_string))


## p. 196 large files: one million digits
filename = 'src/pi_million_digits.txt'

with open(filename) as file_object:
	lines = file_object.readlines()

# create empty string
pi_string = ''

# create empty string
for line in lines:
	pi_string += line.strip() # instead of rstrip

print(pi_string[:52] + '...')
print(len(pi_string))

bday = input('Enter your bday as yyyymmdd: ')

if bday in pi_string:
	print('Yes!')
else:
	print('No!')


### Writing to a File

## p. 197 write to an empty file

filename = 'programming.txt'

with open(filename, 'w') as file_object:
	file_object.write('I love programming!')


## p. 198 write multiple an empty file
filename = 'programming.txt'

# open file for writing
with open(filename, 'w') as file_object:
	file_object.write('I love programming!\n')
	file_object.write('Second line!\n')


## p. 198 append a file
filename = 'programming.txt'

# open file for append
with open(filename, 'a') as file_object:
	file_object.write('Extra text from append!\n')
	file_object.write('One more!\n')


## p. 200 handling the `ZeroDivisioEerror` exception
# print(5/0) # error

# add try-except
try:
	print(5/0)
except ZeroDivisionError:
	print('Cannot divide by zero!')


## p. 201 using exceptions to prevent crashes
print('Enter two numbers. Divide them.')
print('(`q` = quit.)')

while True:
	first_number = input("\nFirst number: ")
	if first_number == 'q':
		break
	second_number = input("\nSecond number: ")
	if second_number == 'q':
		break
	try:
		answer = int(first_number) / int(second_number)
	except ZeroDivisionError:
		print('Cannot divide by zero!')
	else:
		print(answer)


## p. 201 handling the FileNotFoundError / IOError
# alice.py
filename = 'alice.txt'

try:
	with open(filename, encoding='utf-8') as f_obj:
		contents = f_obj.read()
		# p. 204 analyze text
		title = 'Alice in Wonderland'
		print(
			len(contents.split())
		)
except FileNotFoundError:
	print('File not found!')



## p. 205 working with multiple files
def count_words(filename):
	"""Count words in file."""

	try:
		with open(filename, encoding='utf-8') as f_obj:
			contents = f_obj.read()
			# p. 204 analyze text
			#title = 'Alice in Wonderland'
			words = contents.split()
			print(len(words))
	except FileNotFoundError:
		#print('File not found!')
		pass # silent fail

# main: one file
#filename = 'alice.txt'
#count_words(filename)

## p. 206 main: multiple file
filenames = ['alice.txt','siddharta.txt','moby_dick.txt','little_women.txt']
for filename in filenames:
	count_words(filename)
# file not found - three times

## p. 207 deciding which errors to report


### Storing Data

## p. 209 using `json.dump()` and `json.load()`

import json

numbers = [2,3,5,7,11,13]

## dump
filename = 'numbers.json'
with open(filename, 'w') as f_obj:
	json.dump(numbers, f_obj)

## load
with open(filename) as f_obj:
	numbers = json.load(f_obj)
print(numbers)


## p. 210 saving and reading user-generated data

import json

username = input('What is your name? ')

filename = 'username.json'

# dump and notify user
with open(filename, 'w') as f_obj:
	json.dump(username, f_obj)
	print('User', username, 'saved to file!')

# load and hello user
with open(filename) as f_obj:
	username = json.load(f_obj)
	print('Hello, user', username)

## p. 211 saving and reading user-generated data
import json
filename = 'username.json'

try:
	# load and hello user
	with open(filename) as f_obj:
		username = json.load(f_obj)
except FileNotFoundError:
	username = input('What is your name? ')
	# dump and notify user
	with open(filename, 'w') as f_obj:
		json.dump(username, f_obj)
		print('User', username, 'saved to file!')
else:
	print('Welcome back', username)


## p. 212 refactoring
# 1. remember_me.py
import json

def greet_user():
	"""Greet user by name."""
	filename = 'username.json'

	try:
		# load and hello user
		with open(filename) as f_obj:
			username = json.load(f_obj)
	except FileNotFoundError:
		username = input('What is your name? ')
		# dump and notify user
		with open(filename, 'w') as f_obj:
			json.dump(username, f_obj)
			print('User', username, 'saved to file!')
	else:
		print('Welcome back', username)

# main
greet_user()


# 2. refactoring greet_user, which not only greets
# remember_me.py
import json

def get_stored_username():
	"""Get stored username if available."""
	filename = 'username.json'

	try:
		# load and hello user
		with open(filename) as f_obj:
			username = json.load(f_obj)
	except FileNotFoundError:
		return None
	else:
		return username

def greet_user():
	"""Greet user by name."""
	username = get_stored_username()
	if username:
		print('Welcome back', username)
	else:
		username = input('What is your name? ')
		filename = 'username.json'
		# dump and notify user
		with open(filename, 'w') as f_obj:
			json.dump(username, f_obj)
			print('User', username, 'saved to file!')

# main
greet_user()
'''

## p. 213. refactoring greet_user, which not only greets
# remember_me.py
import json

def get_stored_username():
	"""Get stored username if available."""
	filename = 'username.json'

	try:
		# load and hello user
		with open(filename) as f_obj:
			username = json.load(f_obj)
	except FileNotFoundError:
		return None
	else:
		return username


def get_new_username():
	"""Prompt for a new username."""
	username = input('What is your name? ')
	filename = 'username.json'
	with open(filename, 'w') as f_obj:
		json.dump(username, f_obj)
	return username


def greet_user():
	"""Greet user by name."""
	username = get_stored_username()
	if username:
		print('Welcome back', username)
	else:
		username = get_new_username()
		filename = 'username.json'
		# dump and notify user
		with open(filename, 'w') as f_obj:
			json.dump(username, f_obj)
			print('User', username, 'saved to file!')

# main
greet_user()
