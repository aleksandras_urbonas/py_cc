#### Aleksandras Urbonas, 2018-08-09
title = 'Python Crash Course'
chapter = '5'
chapter_name = '`if` statements'
print('~' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')


'''
### completed

## p. 76 simple if statement
cars = [
'ferrari'
, 'mercedes'
, 'bmw'
]

# print with `if` statement
for car in cars:
	if car == 'ferrari':
		print(car.upper())
	else:
		print(car.title())


## p. 77 conditional testing
car = 'Ferrari'
print(car == 'ferrari')
print(car.lower() == 'ferrari')



## p. 78 non-equality
car = 'Audi'
if car != 'BMW':
	print(car, 'is not the same as BMW!')


## p. 79 compare numbers
print(1 > 0)
print(1 != 0)
print(1 != 0)


## p. 80 compare multiple conditions
age = 30
lower_limit = 18
upper_limit = 70
print(age > lower_limit and age < upper_limit)


## p. 81 value in list
listing = ['a', 'b', 'c']
letter = 'b'
print(letter in listing)


## value not in list
seen = ['Matrix', 'Titanic', 'Jurassic Park']
new = 'Invinsibles 2'
if(not new in seen):
	print('Check this new movie out:', new)


## p. 81 boolean
game_started = True
player_active = True

if game_started:
	print('game started!\n')
	if player_active:
		print('player active!\n')
	else:
		print('waiting for player!\n')
else:
	if player_active:
		print('please start the game!\n')
	else:
		print('game is running!\n')


##  p. 83 simple `if`
age = 18
if age < 21:
	print('At age of',age,'you are too young for casino!\n')
	print('Please wait a couple years!\n')


## p. 84 simple if-elif-else
age = 50
age_group = '<undefined>'

if age < 18:
	age_group = 'younger'
elif age > 65:
	age_group = 'elder'
# skipping else can lead to error
else:
	age_group = 'middle'

print(
'age is'
,age
,'age_group is'
,age_group
,'\n'
)


## p. 85 conditions with `in`
order = [
'snacks'
, 'drinks'
]

if 'snacks' in order:
	print('Adding snacks!')
if 'drinks' in order:
	print('Adding drinks!')
if 'extra' in order:
	print('Adding extra!')

print('\n----------------')
print('Order completed!\n')

## p. 88 conditions with `in`
pizza = [
'cheese'
, 'green pepper'
, 'salami'
]

for ingredient in pizza:
	if ingredient == 'green pepper':
		print('_OUT OF STOCK_:', ingredient)
	else:
		print('OK:', ingredient)


## p. 91 check if list is empty
ordered_toppings = []

if ordered_toppings:
	print('add something!')
else:
	print('please confirm if no extras?')


## p. 92 check multiple lists

available_products = [
'cheese'
, 'red pepper'
, 'salami'
]

ordered = [
'cheese'
, 'green pepper'
, 'salami'
]

for ingredient in ordered:
	if ingredient in available_products:
		print('Adding', ingredient)
	else:
		print(ingredient, 'not found!')

print('Pizza completed!')


## p. 93 list exercises
digits = range(0, 10)
which_end = ''

for digit in digits:
	if digit == 1:
		which_end = 'st'
	elif digit == 2:
		which_end = 'nd'
	elif digit == 3:
		which_end = 'rd'
	else:
		which_end = 'th'
	print(str(digit) + which_end)
'''
