#### Aleksandras Urbonas, 2018-08-09
title = 'Python Crash Course'
chapter = '7'
chapter_name = 'User input and `while` loops'
print('~' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')

## p. 117 function `input` (Py2.x: `raw_input`)

'''
### completed
name = input('Please specify your name:\n')
print('Your name is', name)


prompt = 'Please introduce yourself.'
prompt += '\nWhat is your user name? '
prompt += '\n'

name = input(prompt)
print('Hello, ', name)

## p. 119 input for numbers
age = input('How old are you?\n')
print('age is:', age)
# print('age over 18', age >= 18) # error for age

age = input('How old are you?\n')
age = int(age) # update
print('age is:', age)
print('age over 18:', age >= 18)


### p. 120 modulo operator
print('remainder is:', 4 % 3)
print('multiple is:', 8 // 3)




### p. 121 modulo operator in action
number = input('Specify number. Odd/even will be checked: ')
number = int(number)

if number % 2 == 0:
	print('number', number, 'is even')
else:
	print('number', number, 'is odd')
	

### p. 122 loops with `while`

# initiate loop counter
age = 0

# loop statement
while age <= 15:
	print(age)
	# update counter
	age += 1
	print('\tone year has passed!\n')

## extra example:
suma = 0

skaiciai = range(5555, 7400)

for skaicius in skaiciai:
	suma += skaicius

print(suma)


## p. 123 prompt with loop
prompt = 'This program repeats your commands.\n'
prompt += 'Run `quit` to terminate.\n'

# initialize empty message
message = ''

while message != 'quit':
	message = input(prompt)
	
	if message != 'quit':
		print(message)

print('EOE\n')


## p. 124 `while` loops using flags (boolean status)
prompt = 'This program repeats your commands.\n'
prompt += 'Run `quit` to terminate.\n'

# initialize empty message
message = ''
active = True

while active != False:
	message = input(prompt)
	
	if message == 'quit':
		active = False
		print('Program finished!\n')
	else:
		print(message)


## p. 125 use `break` to exit loop
prompt = 'Please enter a city you have already visited:\n'
prompt += '(Enter `quit` to terminate.)\n'

cities = []

while True:
	city = input(prompt)
	
	if city == 'quit':
		break
	else:
		cities.append(city)
		print('I have been to', city)

print('All cities I have visited:')

for city in cities:
	print(city)


## p. 126 continue loop instead of brake
number = 0

while number < 10:
	number += 1
	
	if number % 2 == 0:
		continue
	
	print(number)


## p. 127 avoid infinite loops
x = 1
while x <= 5:
	print(x)
	# x += 1 # forever loop


### Using `while` for lists and dictionaries

## p. 128 Move items from one list to another
# new users list
unconfirmed_users = ['julie','rockie','donny']
# current user list (empty)
confirmed_users = []

print('Before:')
print(len(unconfirmed_users))
print(len(confirmed_users))

# verify all new users until new user list is empty
while unconfirmed_users:
	new_user = unconfirmed_users.pop()
	print('new_user:', new_user)
	confirmed_users.append(new_user)

print('After:')
print(len(unconfirmed_users))
print(len(confirmed_users))

for confirmed_user in confirmed_users:
	print('user confirmed:', confirmed_user)


## p. 129 remove all items from list
# list of pets
pets = ['cat','dog','rabbit','parrot','dog','cat','cat','dog']
# list of items to exclude
regular = ['cat','dog']

# goal: remove all cats
for animal in regular:
	while animal in pets:
		pets.remove(animal)

# check all exotic animals
print(pets)


## p. 130 fill dictionary with user input
# empty dict
responses = {}

# poll status
polling_active = True

while polling_active:
	# ask for name and response
	name = input('\nWhat is your name? ')
	response = input('\nWhich page did you visit today? ')
	
	# save responses in dict
	responses[name] = response
	
	# check if another vote will be done
	repeat = input('\nIs there another voter? (yes / no) ')
	if repeat == 'no':
		polling_active = False

# Polling is complete. Show the results.
print('\nResults:\n')
for name, response in responses.items():
	print(name + ' visited ' + response + ".")
'''
