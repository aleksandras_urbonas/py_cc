## p. 223 a class to test
# survey.py

class AnonymousSurvey():
	"""Collect anonymous answers to a survey question."""
	
	def __init__(self, question):
		"""Store a question, and prepare to store questions."""
		self.question = question
		self.responses = []
	
	def show_question(self):
		"""Show the question."""
		print(self.question)
	
	def store_response(self, new_response):
		"""Store a single response to the survey."""
		self.responses.append(new_response)

	def show_result(self):
		"""Show all the responses."""
		print("Survey results:")
		for response in self.responses:
			print('-', response)
