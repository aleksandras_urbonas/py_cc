#### Aleksandras Urbonas, 2018-08-11
title = 'Python Crash Course'
chapter = '11'
chapter_name = 'Testing your Code'
print('~' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')

'''
### completed

## p. 216 testing a function
# name_function.py
def get_formatted_name(first, last):
	"""Generate a neatly formatted name."""
	full_name = first + ' ' + last
	return full_name.title()


from name_function import get_formatted_name

print('Enter `q` to quit.')

while True:
	first = input('\nEnter your first name: ')
	if first == 'q':
		break
	last = input('\nEnter your first name: ')
	if last == 'q':
		break
	formatted_name = get_formatted_name(first, last)
	print('\tNeatly formatted name: ' + formatted_name + '.')


## p. 217 unit testing using Python's module `unittest`
import unittest # module for testing
from name_function import get_formatted_name # function being tested

# define class for testing
class NameTestCase(unittest.TestCase):
	"""Tests for 'name_function.py'."""
	
	def test_first_last_name(self):
		"""Do names like 'Janis Joplin' work?"""
		formatted_name = get_formatted_name('janis','joplin')
		self.assertEqual(formatted_name, 'Janis Joplin')

# main
unittest.main()
# Output: OK


## p. 218 a failing test
# name_function_middle.py: add middle name
def get_formatted_name(first, middle, last):
	"""Generate a neatly formatted name."""
	full_name = first + ' ' + middle + ' ' + last
	return full_name.title()

# testing
import unittest # module for testing

# define class for testing
class NameTestCase(unittest.TestCase):
	"""Tests for 'name_function.py'."""
	
	def test_first_last_name(self):
		"""Do names like 'Janis Joplin' work?"""
		formatted_name = get_formatted_name('janis','joplin')
		self.assertEqual(formatted_name, 'Janis Joplin')

# main
unittest.main()

# output: FAILED (errors=1)


## p. 219 respond to failed test
# keep the test and change the code!
# 1. update middle to be optional default

# name_function_middle.py: add middle name
def get_formatted_name(first, last, middle=''):
	"""Generate a neatly formatted name."""
	if middle:
		full_name = first + ' ' + middle + ' ' + last
	else:
		full_name = first + ' ' + last
	return full_name.title()

# testing
import unittest # module for testing

# define class for testing
class NameTestCase(unittest.TestCase):
	"""Tests for 'name_function.py'."""
	
	def test_first_last_name(self):
		"""Do names like 'Janis Joplin' work?"""
		formatted_name = get_formatted_name('janis','joplin')
		self.assertEqual(formatted_name, 'Janis Joplin')

# main
unittest.main()
# output: OK!



## p. 221 adding new tests
# testing
import unittest # module for testing
from name_function import get_formatted_name # function being tested

# define class for testing
class NameTestCase(unittest.TestCase):
	"""Tests for 'name_function.py'."""
	
	def test_first_last_name(self):
		"""Do names like 'Janis Joplin' work?"""
		formatted_name = get_formatted_name('janis','joplin')
		self.assertEqual(formatted_name, 'Janis Joplin')

	def test_first_middle_last_name(self):
		"""Do names like 'Wolfgang Amadeus Mozart' work?"""
		# note how middle name is in the end!
		formatted_name = get_formatted_name('wolfgang','mozart','amadeus')
		self.assertEqual(formatted_name, 'Wolfgang Amadeus Mozart')

# main
unittest.main()



### Test a Class
## p. 222 a variety of assert methods in `unittest.TestCase`
#assertEqual(a,b) # a==b
#assertNotEqual(a,b) # a!=b
#assertTrue(x) # x is True
#assertFalse(x) # x is False
#assertIn(item, list) # item in list
#assertNotIn(item, list) # item not in list


## p. 223 a class to test
# survey.py


## p. 224
# main
from survey import AnonymousSurvey

# define a question and make a survey
question = 'What language did you learn to speak?'
my_survey = AnonymousSurvey(question)

# Show the question, and store responses
my_survey.show_question()
print('Enter `q` to quit.\n')

while True:
	response = input("Language: ")
	if response == 'q':
		break
	my_survey.store_response(response)

# Show survey results
print("Thank you for participating in this survey.")
my_survey.show_result()


## p. 225 testing class AnonymousSurvey

# testing
import unittest # module for testing
from survey import AnonymousSurvey # tested module

# define class for testing
class Test_AnonymousSurvey(unittest.TestCase):
	"""Tests for class `AnonymousSurvey`."""
	
	def test_store_single_response(self):
		"""Test if single response is saved properly."""
		question = 'What language did you learn to speak?'
		my_survey = AnonymousSurvey(question)
		my_survey.store_response('English')
		
		self.assertIn('English', my_survey.responses)

	def test_store_multiple_response(self):
		"""Test that several responses are saved properly."""
		question = 'What language did you learn to speak?'
		my_survey = AnonymousSurvey(question)
		responses = ['py','r','scala']
		for response in responses:
			my_survey.store_response(response)
		
		for response in responses:
			self.assertIn(response, my_survey.responses)

# main
unittest.main() # OK

'''


## p. 225 method `setUp()`
# setup run setup for every test_ cases

import unittest # module for testing
from survey import AnonymousSurvey # tested module

# define class for testing
class TestAnonymousSurvey(unittest.TestCase):
	"""Tests for class `AnonymousSurvey`."""
	
	def setUp(self):
		"""Create a survey, set responses for testing of all methods."""
		question = 'What language did you learn to speak?'
		self.my_survey = AnonymousSurvey(question)
		self.responses = ['py','r','scala']
	
	def test_store_single_response(self):
		"""Test if single response is saved properly."""
		self.my_survey.store_response(self.responses[0])
		self.assertIn(self.responses[0], self.my_survey.responses)

	def test_store_multiple_response(self):
		"""Test that several responses are saved properly."""
		for response in self.responses:
			self.my_survey.store_response(response)
		
		for response in self.responses:
			self.assertIn(response, self.my_survey.responses)

# main
unittest.main() # OK
