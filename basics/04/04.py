#### Aleksandras Urbonas, 2018-08-09
title = 'Python Crash Course'
chapter = '4'
chapter_name = 'Looping over Lists'
print('~' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')


'''
### completed
my_list = [
'apples'
,'bananas'
,'water'
,'buckwheat'
]


## p. 54 iterate list
for item in my_list:
	print(item + ', so delicious!')
	print('one more text inside the same iteration')
print('iterations completed')


## p. 55-60 indentation errors


## p. 61 iterate numbers list
for value in range(2, 9):
	print(value)


## p. 62 iterate numbers list every X steps
start = 2
end = 19
every = 3
for value in range(start, end, every):
	print(value)


## p. 62 create list over iterations
squares = []

for number in range(1, 11):
	square = number ** 2
	print(square)
	squares.append(square)

print(squares)


## p. 63 create list over iterations (shorter)
squares = []

for number in range(1, 11):
	squares.append(number ** 2)

print(squares)


## p. 63 simple statistics
digits = []

for number in range(1, 10):
	digits.append(number)

print(digits)

print(min(digits))
print(max(digits))
print(sum(digits))


## p. 64 list comprehension
digits = [digit for digit in range(1, 100)]
print(sum(digits))

# create list and update elements on the go, during creation
digits_2 = [digit * 2 for digit in range(1, 100)]
print(sum(digits_2))


## p. 65 slice a list
digits = [digit for digit in range(1, 100)]
print(digits[1:12])
# from beginning to..
print(digits[:2])
# from .. to end
print(digits[95:])


## p. 66 slice a list from the end
# from .. to end
digits = [digit for digit in range(1, 10)]
print(digits[-3:])
print(digits[:-5])


## p. 67 copy a list
my_list = [1,2,3]
my_list_copy = my_list[:]
print(my_list_copy)


## p. 68 update two lists
my_list = [1,2,3]
#my_list_copy = my_list # does not work, copies a link
my_list_copy = my_list[:]
print(my_list)
my_list.append(4)
print(my_list, '\n')
print(my_list_copy)
my_list_copy.append(5)
print(my_list_copy)


## p. 70 tuples
my_tuple = (1, 2)
print(my_tuple)


## p. 70 tuples
my_tuple = (1, 2)
print(my_tuple)
print(my_tuple[0])
print(my_tuple[1])

# update tuple element by index
#my_tuple[0] = 6 # error
print(my_tuple)

# iterate tuple elements
for element in my_tuple:
	print(element)


## p. 72 style guides
# Python Enhancement Proposal (PEP), currently, PEP 8: https://pep8.org/
# indentation: use four spaces
# line length: 72 (recommended)
# blank lines

'''
