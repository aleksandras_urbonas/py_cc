# Aleksandras Urbonas, 2018-08-09

title = 'Python Crash Course'
chapter = '2'
chapter_title = 'Variables'

''' ### text

# user details
first_name = 'Aleksandras'
last_name = 'Urbonas'
print(first_name.lower())
print(last_name.upper())

# welcome message
message = 'welcome to python'
print(message.title())

# combined message
print(message.title(), ",", first_name.upper(), "!")

# full name
full_name = first_name + ' ' + last_name
# notice simpler printing
print(full_name)


### white spaces
print('\ttabulated text')
print('\nnew line')
print(' space from left!'.lstrip())
print('space from right!'.rstrip())
## trim white spaces
text = ' \t\t\nclean spaces from both sides\n\n     \t\t\t '
print(len(text))
print(len(text.strip()))
'''
''' ### numbers
## integers
print('add: 2 + 2','=', 2 + 2)
print('subtract: 3 - 5','=', 3 - 5)
print('multiply: 8 * 9','=', 8 * 9)
print('divide: 9 / 6','=', 9 / 6)
print('power: 2 ^ 8','=', 2 ** 5)
## operations order
print('2 + 3 * 4','=', 2 + 3 * 4)
print('(2 + 3) * 4','=', (2 + 3) * 4)
## floats
print('0.1 + 0.1','=', 0.1 + 0.1, type(0.1 + 0.1))
print('0.1 + 1','=', 0.1 + 1)
print('round(0.1 + 1)','=', round(0.1 + 1), type(round(0.1 + 1)))
## combine numbers and text
age = 35
# greeting = "Congratulations with your" + age + "rd bday!" # error
greeting = "Congratulations with your " + str(age) + "th bday!"
print(greeting)
'''
### Zen of Python
import this
url = 'https://github.com/python/peps/blob/master/pep-0020.txt'
print('taken from', url)
