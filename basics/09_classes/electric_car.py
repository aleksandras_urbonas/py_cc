from car import Car

class ElectricCar(Car):
	"""Represents aspects of a car, specific to electric cars."""

	def __init__(self, make, model, year):
		"""1. Initialize attributes of parent class."""
		"""2. Initialize attributes of child class."""
		# 1.
		super().__init__(make, model, year)
		# for python 2.7:
		# super(ElectricCar, self).__init__(make, model, year)
		# 2. p. 174
		# self.battery_size = 70
		# 3. p. 176
		self.battery = Battery()
	
	def describe_battery(self):
		"""Provide info about battery."""
		print('Car is equipped with ' + str(self.battery_size) + '-kWh battery!')

	def fill_gas_tank(self):
		"""Electric cars do not have gas tank."""
		print("This is an eletric car and gas tanks are not supported!")


class Battery():
	"""A simple model of electric car battery."""
	
	def __init__(self, battery_size=70):
		"""Initialize battery's attributes."""
		self.battery_size=battery_size
	
	def describe_battery(self):
		"""Print battery size info."""
		print('This battery is ' + str(self.battery_size) + '-kWh!')
	
	def get_range(self):
		"""Estimate battery range in miles."""
		if self.battery_size == 70:
			range = 240
		elif self.battery_size == 85:
			range = 270
		
		message = 'This can go approx. ' + str(range) + ' miles.'
		print(message)
