'''
## p. 179 import class
# car.py
from car import Car
# from car import Car, ElecticCar # two classes in one module

my_new_car = Car('audi', 'a4', 2016)
print(my_new_car.get_descriptive_name())
my_new_car.odometer_reading = 23
my_new_car.read_odometer()

## import entire module
import car

my_1 = car.Car('vw','bettle',2015)
my_1e = car.ElectricCar('vw','e-bettle',2017)

## p. 182 import all classes
# from module_name import * # not recommended


## p. 183 import a module into a module
# see:
# - car.py
# - electric_car.py
# - my_cars.py


## p. 185 standard library
# - favorite_languages.py

'''