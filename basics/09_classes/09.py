#### Aleksandras Urbonas, 2018-08-10
title = 'Python Crash Course'
chapter = '9'
chapter_name = 'Classes'
print('~' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')

'''
### completed

## p. 162 create a class `dog`
class Dog():
	"""A simple attempt to model a dog."""
	
	def __init__(self, name, age):
		"""Initialize name and age attributes"""
		self.name = name
		self.age = age

	def sit(self):
		"""Stimulate a dog sitting in response to a command."""
		print(self.name.title(), 'is sitting!')
	
	def roll_over(self):
		"""Simulate rolling over in response to a command."""
		print(self.name.title(), 'rolled over!')

	def describe(self):
		print(self.name, 'is', self.age)



## p. 164 make a class instance
my_dog = Dog('Klifas', 5)
print(my_dog.name)
print(my_dog.age)


## p. 165 access class attributes
my_dog.sit()
my_dog.roll_over()


## p. 165 create multiple class instances
my_dog = Dog('willie', 6)
your_dog = Dog('lucy', 3)
my_dog.describe()
your_dog.describe()
my_dog.sit()
your_dog.sit()


## p. 167 working with classes and instances
# class `car`
class Car():
	"""A simple attempt to model a car."""
	
	def __init__(self, make, model, year):
		"""Initialize attributes to describe a car."""
		self.make = make
		self.model = model
		self.year = year

	def get_descriptive_name(self):
		"""Return a formatted descriptive name"""
		long_name = str(self.year) + ' ' + self.make + ' ' + self.model
		return long_name.title()

# create new `car`
my_new_car = Car('audi', 'a4', 2016)
print(my_new_car.get_descriptive_name())


## p. 168 set default value for an attribute
# class `car`
class Car():
	"""A simple attempt to model a car."""

	def __init__(self, make, model, year):
		"""Initialize attributes to describe a car."""
		self.make = make
		self.model = model
		self.year = year
		self.odometer_reading = 0 # !

	def get_descriptive_name(self):
		"""Return a formatted descriptive name"""
		long_name = '\n' + str(self.year) + ' ' + self.make + ' ' + self.model
		return long_name.title()

	def read_odometer(self):
		"""Print car mileage"""
		print(self.make.title(), 'has', self.odometer_reading, 'miles on it!')

	def update_odometer(self, mileage):
		"""Update car mileage"""
		if mileage >= self.odometer_reading:
			self.odometer_reading = mileage
		else:
			print('You cannot roll back an odometer!')

	def increment_odometer(self, miles):
		"""Add the given amount to the odometer reading."""
		self.odometer_reading += miles

########################################################################
# create new `car`
my_new_car = Car('audi', 'a4', 2016)
print(my_new_car.get_descriptive_name())
my_new_car.read_odometer()


## p. 169 modify attribute values
# a) directly

my_new_car.odometer_reading = 23
my_new_car.read_odometer() # 23

# b) using method
my_new_car.update_odometer(59)
my_new_car.read_odometer() # 59
my_new_car.update_odometer(23) # message


## p. 170 increment attribute value through a method
my_new_car.increment_odometer(10)
my_new_car.read_odometer() # 69

my_used_car = Car('subaru','outback',2013)
print(my_used_car.get_descriptive_name())

my_used_car.update_odometer(2350)
my_used_car.read_odometer()
my_used_car.increment_odometer(100)
my_used_car.read_odometer()
'''


## p. 172 inheritance
## method `__init__` for a child class
# electric_car.py
# electric_car.py
class Car():
	"""A simple attempt to model a car."""

	def __init__(self, make, model, year):
		"""Initialize attributes to describe a car."""
		self.make = make
		self.model = model
		self.year = year
		self.odometer_reading = 0 # !

	def get_descriptive_name(self):
		"""Return a formatted descriptive name"""
		long_name = '\n' + str(self.year) + ' ' + self.make + ' ' + self.model
		return long_name.title()

	def read_odometer(self):
		"""Print car mileage"""
		print(self.make.title(), 'has', self.odometer_reading, 'miles on it!')

	def update_odometer(self, mileage):
		"""Update car mileage"""
		if mileage >= self.odometer_reading:
			self.odometer_reading = mileage
		else:
			print('You cannot roll back an odometer!')

	def increment_odometer(self, miles):
		"""Add the given amount to the odometer reading."""
		self.odometer_reading += miles


class ElectricCar(Car):
	"""Represents aspects of a car, specific to electric cars."""

	def __init__(self, make, model, year):
		"""1. Initialize attributes of parent class."""
		"""2. Initialize attributes of child class."""
		# 1.
		super().__init__(make, model, year)
		# for python 2.7:
		# super(ElectricCar, self).__init__(make, model, year)
		# 2. p. 174
		# self.battery_size = 70
		# 3. p. 176
		self.battery = Battery()
	
	def describe_battery(self):
		"""Provide info about battery."""
		print('Car is equipped with ' + str(self.battery_size) + '-kWh battery!')

	def fill_gas_tank(self):
		"""Electric cars do not have gas tank."""
		print("This is an eletric car and gas tanks are not supported!")
		
class Battery():
	"""A simple model of electric car battery."""
	
	def __init__(self, battery_size=70):
		"""Initialize battery's attributes."""
		self.battery_size=battery_size
	
	def describe_battery(self):
		"""Print battery size info."""
		print('This battery is ' + str(self.battery_size) + '-kWh!')
	
	def get_range(self):
		"""Estimate battery range in miles."""
		if self.battery_size == 70:
			range = 240
		elif self.battery_size == 85:
			range = 270
		
		message = 'This can go approx. ' + str(range) + ' miles.'
		print(message)


## p. 173 create new electric car
my_tesla = ElectricCar('tesla','model s', 2016)
print(my_tesla.get_descriptive_name())


## p. 174 define atrributes and methods for athe child class
# my_tesla.describe_battery()


## p. 175 override methods from parent class
# fill_gas_tank function

## p. 175 instances as attributes
# class Battery

my_tesla.battery.describe_battery()


## p. 177
my_tesla.battery.get_range()


## p. 179 import class
# car.py
from car import Car
