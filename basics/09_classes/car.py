class Car():
	"""A simple attempt to model a car."""

	def __init__(self, make, model, year):
		"""Initialize attributes to describe a car."""
		self.make = make
		self.model = model
		self.year = year
		self.odometer_reading = 0 # !

	def get_descriptive_name(self):
		"""Return a formatted descriptive name"""
		long_name = '\n' + str(self.year) + ' ' + self.make + ' ' + self.model
		return long_name.title()

	def read_odometer(self):
		"""Print car mileage"""
		print(self.make.title(), 'has', self.odometer_reading, 'miles on it!')

	def update_odometer(self, mileage):
		"""Update car mileage"""
		if mileage >= self.odometer_reading:
			self.odometer_reading = mileage
		else:
			print('You cannot roll back an odometer!')

	def increment_odometer(self, miles):
		"""Add the given amount to the odometer reading."""
		self.odometer_reading += miles
