## p. 185 standard library
# favorite_languages.py
# use module `collections`, function for `OrderedDict`
from collections import OrderedDict

fav_lang = OrderedDict()

fav_lang['alex'] = 'r'
fav_lang['christie'] = 'python'
fav_lang['julie'] = 'spark'
fav_lang['vaidas'] = 'scala'

for name, language in fav_lang.items():
	print(
		name.title() 
		+ "'s favorite language is "
		+ language.title()
	)
