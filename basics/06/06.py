#### Aleksandras Urbonas, 2018-08-09
title = 'Python Crash Course'
chapter = '6'
chapter_name = 'Dictionaries'
print('~' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')

'''
## p. 96
# create empty dictionary
my_dict = {}

# create dictionary
my_dict = {'id': 1, 'name': 'Aleksandras'}

# print dictionary
print(my_dict)

# access dictionary elements
print(
'id:'
, my_dict['id']
, '\t'
, 'name:'
, my_dict['name']
)

## p. 97 add element
# new empty dictionary
my_dict = {}

# new property
my_dict['age'] = 35
my_dict['gender'] = 'male'
print(my_dict)


## p. 98 modify dictionary
# new empty dictionary
my_dict = {}

# new property
my_dict['age'] = 35
my_dict['gender'] = 'male'
print(my_dict)

# new property
my_dict['age'] = 36
print(my_dict)


## p. 99 aliens
time = range(0, 1000)

alien_0 = {'color': 'yellow', 'x_coord': 0, 'y_coord': 25, 'speed': 'slow'}

print(alien_0)

for moment in time:
	if alien_0['speed'] == 'slow':
		increment = 1
	elif alien_0['speed'] == 'medium':
		increment = 2
	elif alien_0['speed'] == 'fast':
		increment = 3

	if moment == 998:
		alien_0['speed'] = 'fast'
		print('moving faster!')

	alien_0['x_coord'] = alien_0['x_coord'] + increment
	alien_0['y_coord'] = alien_0['y_coord'] + increment

	print(alien_0['x_coord'], alien_0['y_coord'])

print('move completed!')



## p. 100 remove key-pair from dictionary

# create dictionary
my_dict = {'salary': 100, 'debts': 200}
print('full list:\n', my_dict)

# delete by key
del my_dict['debts']
print('key removed:\n', my_dict)


## p. 103 iterate keys and values

# create
my_dict = {'salary': 100, 'debts': 200}

# iterate
for key, value in my_dict.items():
	print(
	key
	, value
	)


## p. 105 loop all keys
# create
py_hours = {
'Kristina': 100
, 'Aleksandras': 200
, 'Julija': 50
}

# iterate
for key in py_hours.keys():
	print(key)


## p. 105 dictionary and list
# create dict
py_hours = {
'Kristina': 100
, 'Aleksandras': 200
, 'Julija': 50
}

# create list
adults = ['Aleksandras','Kristina']

for name in py_hours.keys():
	print(name.title())

	if name in adults:
		print('Adult, worked', py_hours[name])
	else:
		print('Kids are not accounted!\n')


## p. 106 loop dictionary values
# create dict
py_hours = {
  'Kristina': 100
, 'Aleksandras': 200
, 'Aleksandras': 100
, 'Julija': 50
}

print('print all items:\n')
for value in py_hours.values():
	print(value)

print('using `set`:\n')
for value in set(py_hours.values()):
	print(value)



## p. 109 list of dictionaries
alien_0 = {'color': 'green', 'size': 1}
alien_1 = {'color': 'green', 'size': 2}
alien_2 = {'color': 'blue', 'size': 4}

aliens = [
alien_0
, alien_1
, alien_2
]

print(aliens)


## p. 109 generating lists of dictionaries

# empty list
aliens = []

# create 20 green aliens
for counter in range(20):
	print(counter)
	alien_0 = {'color': 'green', 'size': 1}
	aliens.append(alien_0)
	
print('Aliens created\n')

print(len(aliens))

# update first three elements
for alien in aliens[:3]:
	print(alien)
	if alien['color'] == 'green':
		alien['color'] = 'yellow'
		alien['size'] = 3

print(aliens[0:5])



## p. 111 list in dictionary
# create detailed order
pizza_order = {
'crust': 'thick'
, 'toppings': ['cheese', 'ham']
}

print(pizza_order)

print(
'You have ordered a '
+ pizza_order['crust']
+ '-crust pizza with the following toppings:\n'
)

for item in pizza_order['toppings']:
	print(item)


## p. 112 lists in dictionary
favorite_language = {
'Aleksandras': ['R', 'python']
, 'Kristina': ['java', 'python']
, 'Julija': []
}


print(favorite_language)

# iterate
for name, values in favorite_language.items():
	print('\n' + name.title() + ':')
	
	# check if language lit is not empty
	if not values:
		print('> none so far!')
	else:
		for language in values:
			print('>' + language.title())

'''


## p. 113 dictionary in dictionary
# create dictionary
users = {
	'aurbonas': {
		'first_name': 'Aleksandras'
		, 'last_name': 'Urbonas'
		, 'dob': '19830905'
	}
, 	'rurbonas': {
		'first_name': 'Rimvydas'
		, 'last_name': 'Urbonas'
		, 'dob': '19591121'
	}
}

print(users)
