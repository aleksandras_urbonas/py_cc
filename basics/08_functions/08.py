#### Aleksandras Urbonas, 2018-08-09
title = 'Python Crash Course'
chapter = '8'
chapter_name = 'Functions'
print('~' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')


'''
### completed


## p. 134 define a function
def greet_user():
	"""Display a simple greeting"""
	print("Hello!")

greet_user()

## p. 134 define a function with parameter
def greet_user(name):
	"""Display a simple greeting"""
	print("Hello, " + name.title() + "!\n")

greet_user('alex') # note the title case :)


## p. 135 positional arguments
def describe_pet(animal_type, pet_name):
	"""Display information about a pet."""
	print('\nI have a ' + animal_type + '.')
	print('My ' + animal_type + "'s name is " + pet_name.title() + ".")

describe_pet('cat','Mikis')
# multiple calls
describe_pet('dog','Klifas')


## p. 137 order of positional arguments
# order matters
describe_pet('Mikis','cat')

## p. 137 keyword arguments (name-value pair)
def describe_pet(animal_type, pet_name):
	"""Display information about a pet."""
	print('\nI have a ' + animal_type + '.')
	print('My ' + animal_type + "'s name is " + pet_name.title() + ".")

describe_pet(animal_type = 'cat', pet_name = 'Mikis')

# order does not matter now
describe_pet(pet_name = 'Mikis', animal_type = 'cat')


## p. 138 default values
# dog by default
def describe_pet(pet_name, animal_type = 'dog'):
	"""Display information about a pet."""
	print('\nI have a ' + animal_type + '.')
	print('My ' + animal_type + "'s name is " + pet_name.title() + ".")

describe_pet(pet_name = 'Klifas')
# add extra param: specify explicitly
describe_pet(pet_name = 'Mikis', animal_type = 'cat')


## p. 139 equivalent function calls
def describe_pet(pet_name, animal_type = 'dog'):
	"""Display information about a pet."""
	print('\nI have a ' + animal_type + '.')
	print('My ' + animal_type + "'s name is " + pet_name.title() + ".")

# no parameter
describe_pet('Klifas') # works
describe_pet(pet_name = 'Klifas') # works

# two parameters, no positional arguments
describe_pet('Mikis','cat') # works
# two parameters, with positional arguments
describe_pet(pet_name = 'Mikis', animal_type = 'cat') # works
describe_pet(animal_type = 'cat', pet_name = 'Mikis') # works


## p. 140 avoid argument errors

def describe_pet(pet_name, animal_type = 'dog'):
	"""Display information about a pet."""
	print('\nI have a ' + animal_type + '.')
	print('My ' + animal_type + "'s name is " + pet_name.title() + ".")

# no params leads to an error: missing parameter
# describe_pet()



## p. 142 return values
def format_name(first_name, last_name):
	"""Return full name, neatly formatted."""
	full_name = first_name + ' ' + last_name
	return full_name.title()

musician = format_name('jimi','hendrix')
print(musician) # Jimi Hendrix



## p. 142-143 optional argument
# some names do not have a middle name: set default value
def format_name(first_name, last_name, middle_name=''):
	"""Return full name, neatly formatted."""
	if middle_name:
		full_name = first_name + ' ' + middle_name + ' ' + last_name
	else:
		full_name = first_name + ' ' + last_name
	return full_name.title()

psychologist = format_name('jordan','b','peterson')
print(psychologist) # Jordan B Peterson

musician = format_name('jimi','hendrix')
print(musician) # Jimi Hendrix


## 144. return a dict
def build_person(first_name, last_name):
	"""Return a dictionary with info about a person"""
	person = {'first':first_name,'last':last_name}
	return person

musician = build_person('jimi','hendrix')
print(musician) # Jimi Hendrix


## 144. return a dict II
# add age with default as empty
def build_person(first_name, last_name, age=''):
	"""Return a dictionary with info about a person"""
	person = {
		'first': first_name
		, 'last': last_name
	}
	if age:
		person['age'] = age
	return person

musician = build_person('jimi','hendrix', age = 27)
print(musician) # Jimi Hendrix


## p. 145 use a functional `while` loop
def format_name(first_name, last_name):
	"""Return full name, neatly formatted."""
	full_name = first_name + ' ' + last_name
	return full_name.title()

# infinite loop!
while True:
	print('\nyour name?:')
	print('\nenter `q` to quit at any time?:')
	# define break condition
	f_name = input('first name: ')
	if f_name == 'q':
		break # or continue!

	l_name = input('last name: ')
	if l_name == 'q':
		break # 
	
	formatted_name = format_name(f_name, l_name)
	print('\nHello, ' + formatted_name + '!')


## p. 147 pass a list
def greet_user(names):
	"""Display a simple greeting for a list of names"""
	for name in names:
		msg = "Hello, " + name.title() + "!\n"
		print(msg)

usernames = ['a','u','b']
greet_user(usernames)


## p. 147 modify a list
unprinted_designs = ['iphone case','robot pendant','dodecahedron']
completed_models = []

# simulate printing, until none are left
while unprinted_designs:
	current_design = unprinted_designs.pop()
	
	# simulate 3d printing
	print('printing:',current_design)
	completed_models.append(current_design)
	
# Display completed models
print('\nThese models have been printed:')
for completed_model in completed_models:
	print(completed_model)


## p. 148 modify a list in function
print('\nModify list using functional approach:\n')

## define functions
# func1
def print_models(unprinted_designs, completed_models):
	"""
	Simulate printing each design, until none are left.
	Move each design to completed_models after printing.
	"""
	while unprinted_designs:
		current_design = unprinted_designs.pop()
		# simulate 3d printing
		print('printing:',current_design)
		completed_models.append(current_design)

## func2
def show_completed_models(completed_models):
	"""Show all printed models."""
	print('\nThese models have been printed:')
	for completed_model in completed_models:
		print(completed_model)

## main
unprinted_designs = ['iphone case','robot pendant','dodecahedron']
completed_models = []

print_models(unprinted_designs, completed_models)
show_completed_models(completed_models)


## p. 150 prevent a function from modifying a list
## define functions
# func1
def print_models(unprinted_designs, completed_models):
	"""
	Simulate printing each design, until none are left.
	Move each design to completed_models after printing.
	"""
	while unprinted_designs:
		current_design = unprinted_designs.pop()
		# simulate 3d printing
		print('printing:',current_design)
		completed_models.append(current_design)

## func2
def show_completed_models(completed_models):
	"""Show all printed models."""
	print('\nThese models have been printed:')
	for completed_model in completed_models:
		print(completed_model)

## main
unprinted_designs = ['iphone case','robot pendant','dodecahedron']
completed_models = []

print('\na list sent to a function will be changed: use a copy!')
print_models(unprinted_designs[:], completed_models)
show_completed_models(completed_models)


## p. 151 pass an arbitrary number of arguments
# pizza can have several toppings and it is unknown in the beginning
def make_pizza(*toppings):
	print('\nToppings have been requested:')
	for topping in toppings:
		print('-', topping)

make_pizza() # works
make_pizza('cheeze') # tuple
make_pizza('ham','mushrooms') # tuple


## p. 152 positional and arbitrary arguments
# pizza can have several toppings and size
def make_pizza(size, *toppings):
	"""Summarize pizza."""
	print(
	'Making a pizza '
	+ str(size)
	+ '-cm pizza with the following ingredients')
	for topping in toppings:
		print('-', topping)

make_pizza(12) # works
make_pizza(12, 'cheeze') # tuple
make_pizza(15, 'ham','mushrooms') # tuple


## p. 153 arbitrary keyword arguments
def build_profile(first, last, **user_info):
	"""	Build a dictionary with all user info."""
	profile = {}
	profile['first_name'] = first
	profile['last_name'] = last
	for key, value in user_info.items():
		profile[key] = value
	return profile

user_profile = build_profile(
'albert', 'einstein'
, location = 'princeton'
, field = 'physics'
)

print(user_profile)
'''


### store functions in modules

## p. 154 import entire module
def make_pizza(
	size
	, *toppings
	):
	"""Summarize pizza."""
	print(
		'Making a pizza '
		+ str(size)
		+ '-cm pizza with the following ingredients'
	)
	for topping in toppings:
		print('-', topping)
