#### Aleksandras Urbonas, 2018-08-09
title = 'Python Crash Course'
chapter = '8'
chapter_name = 'Functions'
print('~' + title + '. Chapter ' + chapter + ': ' + chapter_name , '\n')

'''
#### Part 2
### completed
# use the same name as file containing cuntion
import py8

# call using notation `module_name.function_name()`
py8.make_pizza(12, 'cheeze')
py8.make_pizza(15, 'cheeze', 'ham')


## p. 156 import specific functions
# from `module_name` import `function_name`
from py8 import make_pizza

# call using notation `function_name()`
make_pizza(12, 'cheeze')
make_pizza(15, 'cheeze', 'ham')


## p. 156 import specific functions with alias
# from `module_name` import `function_name` as `fn`
from py8 import make_pizza as mp

# call using notation `function_name()`
mp(12, 'cheeze')
mp(15, 'cheeze', 'ham')


## p. 157 give module an alias
# import `module_name` as `mn`
import py8 as p

# call using notation `mn`.`function_name`()
p.make_pizza(12, 'cheeze')
p.make_pizza(15, 'cheeze', 'ham')


## p. 157 import all functions with alias
# import `module_name` as `mn`
from py8 import *

# call using notation `mn`.`function_name`()
make_pizza(12, 'cheeze')
make_pizza(15, 'cheeze', 'ham')
'''


## p. 158 styling functions
# - functions and modules should have descriptive name
# - for names, use lowercase and underscores
# - comments in docstring format
# - no spaces between '=' sign
# -- for function definitions
# def function_name(param_0, param_1='default_value')
# -- or function calls
# function_name(param_0, param_1='default_value')
# - use pep8: wrap code at 79
# - import goes first
# - functions can be separated using mutiple lines
