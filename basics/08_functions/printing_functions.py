## p. 150 prevent a function from modifying a list
## define functions
# func1
def print_models(unprinted_designs, completed_models):
	"""
	Simulate printing each design, until none are left.
	Move each design to completed_models after printing.
	"""
	while unprinted_designs:
		current_design = unprinted_designs.pop()
		# simulate 3d printing
		print('printing:',current_design)
		completed_models.append(current_design)

## func2
def show_completed_models(completed_models):
	"""Show all printed models."""
	print('\nThese models have been printed:')
	for completed_model in completed_models:
		print(completed_model)
