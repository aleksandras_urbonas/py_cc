import printing_functions as pf

## main
unprinted_designs = ['iphone case','robot pendant','dodecahedron']
completed_models = []

print('\na list sent to a function will be changed: use a copy!')
pf.print_models(unprinted_designs[:], completed_models)
pf.show_completed_models(completed_models)
