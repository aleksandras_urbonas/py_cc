# PY_CC: Python Crash Course

Repository contains code covering the book "Python Crash Course"

- Basics: data types, functions, operations
- Project 1: Alien Invasion / completed
- Project 2: Data Visualization / on hold
- Project 3: Web Applications / in progress

(c) 2018. Aleksandras Urbonas